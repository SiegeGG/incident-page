require 'gitlab'
require 'json'

Gitlab.configure do |config|
    config.endpoint       = 'https://gitlab.com/api/v4'
    config.private_token  = ENV["GITLAB_TOKEN"]
end

def incidents
    issues = Gitlab.issues(16532318, labels: 'incident', per_page: 50, confidential: false).map { |incident| incident.to_h }

    arr = []

    for issue in issues
        created_at = DateTime.parse(issue['created_at'])
        if created_at >= DateTime.now - 14
            arr.push({
                title: issue['title'],
                created_at: issue['created_at'],
                closed_at: issue['closed_at'],
                assignee: issue['assignee'],
                description: issue['description'],
                state: issue['state']
            })
        end
    end

    arr
end

Dir.mkdir('public') unless Dir.exist? 'public'

index = File.open('src/index.html').read

index.gsub!('$INCIDENTS', incidents.to_json)

File.open('public/index.html', 'w+').write(index)
